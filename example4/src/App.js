import './App.css';

import NavBar from './components/NavBar';
import Card from "./components/Card";
import data from "./data";


function App() {
  const cards = data.map(item => {
    return (
        <Card 
            key={item.id}
            {...item}
        />
    )
  })   
  return (
    <div>
      <NavBar/>
      
      {cards}
    </div>
  );
}

export default App;
