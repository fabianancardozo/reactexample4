import React from 'react';
import { IoEarth } from "react-icons/io5";

const styles = {
    nav:{
        width: '100%',
        height: '55px',
        left: '0px',
        top: '0px',
        marginBottom: '10px',

        background: '#F55A5A',
    },
    earth:{
       
        marginTop: '10px',
        fontSize: '40px',
        color:'#FFFFFF',
    },
    title:{
        //width: '109px',
        //height: '16px',
        //left: '244px',
        //top: '20px',
        textAlign:'center',
        marginTop: '-20px',

        fontFamily: 'Inter',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: '30px',
        lineHeight: '18px',
        letterSpacing: '-0.075em',

        color: '#FFFFFF',
    },
}
export default function NavBar() {
  return (
    <nav style={styles.nav}>
        <IoEarth style={styles.earth} />
        <h1 style={styles.title}>my travel journal.</h1>
    </nav>
    );
}
