import React from 'react';
import { GoLocation } from "react-icons/go";

const styles= {
  container:{
    display: 'flex',
  },
  image:{
    width: '125px',
    height: '168px',
    left: '40px',
    top: '100px',
    marginRight: '10px',

    borderRadius: '5px',
  },
  section:{

  },
  icon:{
    width: '7px',
    height: '9.55px',
    left: '184px',
    top: '120px',
    marginRight: '5px',
   

    color: '#F55A5A',

  },
  location:{
    width: '40px',
    height: '10px',
    left: '195px',
    top: '119px',
    marginRight: '5px',

    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '10.24px',
    lineHeight: '12px',
    letterSpacing: '0.17em',

    color: '#2B283A',
  },
  url:{  
    width: '106px',
    height: '13px',
    left: '247px',
    top: '119px',

    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '10.24px',
    lineHeight: '12px',


    textDecorationLine: 'underline',

    color: '#918E9B',

  },
  title:{
    width: '326px',
    height: '40px',
    left: '184px',
    top: '136px',

    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '25px',
    lineHeight: '30px',

    color: '#2B283A',

  },
  start:{
    width: '319px',
    height: '11px',
    left: '184px',
    top: '183px',

    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '10.24px',
    lineHeight: '12px',

    color: '#2B283A',
  },
  description:{
    width: '326px',
    height: '45px',
    left: '184px',
    top: '203px',

    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '10.24px',
    lineHeight: '150%',

    color: '#2B283A',
  },
}


export default function Card(props) {
    
  return (
    <div style={styles.container}>
      <div>
        <img src={require(`../../public/images/${props.coverImg}`)} style={styles.image} />
      </div>

      <div>
        <div>
          <GoLocation style={styles.icon} />
          <span style={styles.location}>{props.location}</span>
          <span style={styles.url}>{props.googleMapsUrl}</span>
        </div>
        <h1 style={styles.title}>{props.title}</h1>
        <div>
          <span style={styles.start}>{props.startDate}</span>
          <span style={styles.start}>{props.endDate}</span>
        </div>
        <p style={styles.description}>{props.description}</p>
      </div>

    </div>

    );
}

   
 